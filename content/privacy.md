+++
comments = false
draft = false
noauthor = true
share = false
title = "Privacy Policy"
type = "page"
+++

This privacy policy explains how any information you provide to StraightFwd websites is used and protected, and why it is collected.

### Gaining your consent

When you use the services of StraightFwd websites you will be informed how the information you provide will be used, stored and destroyed. 


### What information is collected

* Name
* Address
* Email
* Details requried to process your payments

This information might be obtained in person, by phone, the contact form or by email.
___

Each time you visit this site, the following information may be automatically collected: 

* **Technical information** - including the Internet Protocol (IP) address used to connect your computer to the internet; your browser type and version; the time zone setting; and your operating system and platform
* **Information about your visit** - including your clicks to, through and from this site; the pages you view or search for; the length of your visit to certain pages; and your page interaction information, such as scrolling and link clicks.

### What your information is used for
Your information will be used to

* Respond to any enquiries you make
* Track website usage 

### How to access and amend your information
You can request access to the information held about you. You may also ask for this personal information to be amended or deleted.

### Who to contact
In accordance with the General Data Protection Regulation (GDPR), the data controller for Straightfwd websites is Howard Colin. Who responsible for collecting and processing your personal information. You can make contact regarding these matters via the form on our contact page.