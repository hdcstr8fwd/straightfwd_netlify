+++
title = "HTTP Error Codes"
author = "Straightfwd"
description = "HTTP Error codes, explained. As a user of various services on the Internet, you may well have crossed  paths with the dreaded HTTP Error code page. Think of them like the Windows blue-screen of death, but the Internet version."
tags = [
    "internet",
    "development",
]
date = "2018-05-20"
categories = [
    "Development",
    "Design",
]
nomenu = "main"
image= "img/error.png"
+++

As a user of various services on the Internet, you may well have crossed  paths with the dreaded HTTP Error code page. Think of them like the Windows blue-screen of death, but the Internet version.  Don’t worry though, not all are bad and they certainly don’t impact the performance or usability of your computer. 

Here are some of the most common HTTP errors that you might come across on your Internet travels. We have included a short description about each.

## Common Client status codes

### 400 - Bad Request

This status is triggered when the server cannot process the request from the client (you) this could be because of a malformed request.

### 401 - Unauthorized

You need authorization to access the URL, some servers issue this error if your IP has been banned from accessing the website.

### 403 - Forbidden

Similar to 401 but the user does not have permissions to view the current resource.

### 404 - Not Found

The most common status an Internet surfer might experience. The page you are looking for was not found. This is because the URL has been mis-typed or the resource is not available but might be in the future.

## Common server side status codes

### 500 - Internal Server Error

A generic status that can be caused by an unexpected condition that was encountered and no other status message was suitable. 

### 502 - Bad Gateway

This status code is thrown when the server is acting as a gateway or a proxy and received an invalid response. Basically one server on the Internet received an incorrect response from another server. 

### 503 - Service Unavailable

The server is unavailable, this could be because the server is down or overloaded or unreachable because it is down for maintenance.

At Straightfwd we offer custom designed 404 pages for your website. It’s very common for end users to mis-type a URL when visiting your site so it is essential that you have a good 404 landing page to get the visitors back on track. 

---

Should you worry about these potential errors?

For the best part, no. Most of these errors are only temporary. If you run a dynamic website where the content is generated via an application such as Wordpress, you maybe slightly more at risk from 500 series errors as a lot of the processing happens on the server. If you do encounter these status codes they can be fixed fairly quickly.
