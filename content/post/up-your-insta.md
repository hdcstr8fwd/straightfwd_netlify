---
author: "Straightfwd"
date: 2018-05-20
linktitle: Up Your Instagram Game
nomenu:
  main:
    parent: tutorials
next: /tutorials/github-pages-blog
prev: /tutorials/automated-deployments
title: Up Your Instagram Game
weight: 10
image: img/instagram.jpg
authorAvatar: img/ghost-icon.png
---

We love Instagram, as do millions of other people. It has become a house hold name since it was first released in 2010 by Kevin Systrom and Mike Krieger. 

With the end of 2017 looming on us, it’s rare not to find a business worth it's salt on Instagram. It’s a great platform to show off your products and skills. 

With all this in mind, we have got some tips you can take into 2018 that will help make your account grow. Check them out below. 


#### Post regularly

 Keep a regular posting schedule on your Instagram account. If it’s a new account try to post at least 10 images as you start it. Otherwise you should be posting at least once a day.

#### Decide on a colour scheme
If you take a look at a popular account on Instagram, you may find that all their photos follow the same colour scheme. Each consecutive post has a color style in common with the previous. By doing this it creates a much more aesthetically pleasing experience. You use tools such as [Adobe Kuler](https://color.adobe.com/) to help see which colors work well together before you upload your shots.

#### Don’t upload candid shots.

It’s important to only upload your highest quality photographs on your Instagram page. If your image does not follow these guidelines you should not post it on Instagram, keep it for Facebook or Twitter. 

#### Use all the hashtags you can

Research the most relevant hashtags to your brand and use them. Use Google to help you find popular hashtags.  You can also follow your competitors accounts and see what hashtags they are using and use them on your own images. Instagram gives you 30 hashtags per post. Use them all, and use them well.

#### Follow every genuine account that follows you.

If someone follows you, and they are a genuine account, follow them back. Like some of their photos and add some comments, its a two way process.

#### Interact with other accounts

Just because somebody doesn't follow you on Instagram doesn't mean you can’t like or comment their photos. Have a look at the top posts in your most relevant hashtags, try and like and comment on some of their content, it does go a long way.

--- 

It takes a fair bit of effort and a little bit of time, but if you keep this strategy up you will find a massive increase in your Instagram account followers and interaction. 

Is there a tipping point? We have always wondered this. If your account gets a massive surge in posts. Likes, Follows and Comments will follow, we believe that if you apply this technique in combination with the techniques listed above then the engagement will increase in a much greater way than just posting a few times a week or once or twice per day. We haven’t tried it yet, but if anybody has, or would be interested in finding out our results. 
