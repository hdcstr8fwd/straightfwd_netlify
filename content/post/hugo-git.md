+++
title = "Building and deploying with Hugo and Git"
author = "Straightfwd"
description = "Developing and deploying your website through Hugo and Git. The push to deploy method. Hugo is a modern, well documented tool for static site generation. It is written in Go and uses a simple template markup to allow users to create themes."
tags = [
    "git",
    "design",
    "hugo",
    "development",
]
date = "2018-04-20"
categories = [
    "Development",
    "Web Design",
]
nomenu = "main"
image= "img/hugo.png"
+++

[Hugo](http://www.gohugo.io) is a modern, well documented tool for static site generation. It is written in Go and uses a simple template markup to allow users to create themes. Content is managed through Markdown files, which are processed by the system and a flat html file is generated.

This site is built on Hugo as are several of our clients. It is our go-to option for devloping solutions for clients.

### Why not Wordpress?

Good question, why not Wordpress? Or any other CMS for that matter? 

Let us explain:

* Hugo allows us to create blazing fast websites that run on minimal resources.
* There is no need for software updates
* Far less security issues if your website is a flat html file. No complicated php that connects to databases, which hackers search for vunerabilites.
* Less baggage. Complicated CMS systems often come with a lot of features that the end user does not need. Which in turn impacts on page load time and server resources.

### Deploying via Git

One of the great features of Hugo and working with static sites, is being able to store your public files in a Git repository. Any changes made locally can be pushed to the repo stored on Github or Bitbucket. A server set up with the appropriate webhooks can pull from the remote repository everytime an update is pushed, thus automaticlly deploying your website changes for you.

### It's not perfect

There will be times, however. Where static site generators like Hugo just don't fit the type of project you are trying to create. If a website requires multiple authors, or a client wants to manage their own content, another solution would be required. However for most websites it is a valuable tool that allows developers to create amazing websites with the functionality of a larger CMS system.

There are many static site generators out there, you can find a list of the most popular here: [StaticGen](https://www.staticgen.com/)

We have put together a boilerplate Git Repository that is set up to start developing with Hugo and Foundation. You can download it here: [Foundation Theme](https://github.com/hdcdstr8fwd/foundation-theme)

To install this theme run the following commands in your working directory.

    hugo new site examplesite

Navigate to your themes directory in your new hugo site and clone our repo.

    git clone https://github.com/hdcdstr8fwd/foundation-theme

Update your config.toml in your base hugo site to include the theme, save and run:

    hugo serve

You should be able to see your new base theme

If you have any comments or would like to share usefull information please write your message in the comments section below. 
