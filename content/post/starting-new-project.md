+++
author = "Howard"
categories = ["Development", "Design"]
date = "2018-05-31T12:40:05+00:00"
description = "What to expect when starting a new project with Straightfwd."
image = "/img/jesse-orrico-62699-unsplash.jpg"
nomenu = "main"
tags = ["internet", "Design,", "Development"]
title = "Working With Us"
undefined = ""

+++
Starting a new project is really what inspires us, we love to get into the nitty-gritty tech side of things to help create something that is cutting edge, yet as affordable as can be.

When working on a new project we will work with you to get an understanding of what it is you want to create. We are happy to take any design inspiration from you. This might be in the form of print, images or even other websites that you like.

You may be after a landing page, blog or fully fledged multi user content platform. We take your thoughts and ideas on board.

We start the design process by sketching out wire-frames of what the website will look like. These designs are interactive and sent to you via the web. You'll be able to get a pretty good impression of what the finished product will look like.

If you want you can even suggest improvements and changes. Speak now, or forever hold your peace. As once the design is locked off, it can be quite tricky to change things in code.

So, some time has passed… We have been busy working away coding up  your website. It looks great and you are really pleased with it.  Awesome, now we move on to the next step.

## Deployment

Depending on your project there are a number of solutions we love to work with.

We try not to rely on a traditional server / shared hosting platform as the performance of these systems seems to be hit and miss. Though if your project would benefit from this set up then we have good connections with reliable providers.

We love working with cloud based solutions, such as [Netlify](). We find we can build rock solid websites using our favourite static site generator Hugo. For dynamic websites and blogs, we pair this with Forestry.io, which allows clients to log in and edit their own content and posts.

## Continuing support

We like to keep our clients websites running as well as possible. Websites are somewhat similar to cars. In the sense that, to keep them running well they need the occasional service and upgrade. 

We offer a number of service plans that cater for clients of all sizes. By keeping your website up to date with the latest advancements in technology, you will find that the return on investment is greatly increased.