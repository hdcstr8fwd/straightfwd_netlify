+++
title = "5 tips to make your business grow"
author = "Straightfwd"
description = "5 must do tips to make your business grow in 2018. We have put our heads together to create a list of must do things that will help to make your business thrive, less chit chat, Lets get stuck in."
tags = [
    "business",
    "marketing",
    "growth",
]
date = "2018-05-20"
categories = [
    "Business",
    "Startup",
]
nomenu = "main"
image= "img/rich.png"
+++

We have put our heads together to create a list of must do things that will help to make your business thrive, less chit chat, Lets get stuck in. 

### A Responsive Design

This should be a no-brainer in the current day and age. A huge source of your business traffic will be coming from mobile phones and tablets. If your website is not configured to be displayed correctly on smaller screens, you will be at a huge disadvantage to your competitors. In this day and age it should come as standard with any web-design package, thankfully here at Straightfwd Websites it does. 

### Have a Clear Plan 

Make sure that you have a clear plan to see your business into the future. We are thinking a 2 to 5 year plan depending on your circumstances. Without a plan,how are you ever going to get to where you want? Make a simple plan that you can refer to every few months throughout the year to make sure that you stay on track. If you can get a business mentor to help with this then that would be even better, knowledge is better shared. 

### Focus your attention on Social Media 

Facebook and Twitter offer great platforms for businesses to get there messages out there. Have a look at the keywords that your target customer is using. Create engaging, original content that fits those keywords. Get that content in front of them, link back to your business and make sure that it's of real value to your customers. It will raise your profile as a brand, making you much more respected.
 
### Social Media Ads

In relation to the above post, many of the larger social media platforms offer a advertising service. Boosted posts, and paid adverts allow any business or individual to reach out to a group of people. You can set your budget from anything to £10 per week or £500 a day. Using this wisely with section 3 from this post will help you streamline your social media. 

### Mailing Lists and Email Newsletters 

Allow your target customers to sign up to your mailing list service. Send out a monthly newsletter with interesting articles and ideas from yourself as well as engaging content from other creators on the internet. If you do it right, you can get a huge following. A great example is from [https://hiutdenim.co.uk/](Hiut Denim), their scrapbook chronicles is a weekly newsletter that is great, it shows interesting content that has been found online that matches their customer base, along with news about their own products. 

The Straightfwd newsletter will be coming soon, we will be talking web, design and business ideas on it. If this is something that interests you post a comment below.

