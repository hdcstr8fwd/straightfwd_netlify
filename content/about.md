+++
comments = false
draft = false
noauthor = true
share = false
title = "About Us"
type = "page"
[menu.main]
weight = 111

+++

Straightfwd is a web design and digital agency based in South Wales. We work with clients big and small, on a range of projects. 

We pride ourselves on building engaging web-sites and branded content for clients. We aim to provide an exceptional level of service to our clients, that you just wouldn't get with a large scale design agency.

We live and work by the following: 

* ### Always answer the phone and reply to emails
We admit, sometimes this is hard. If you can't get through to us via email then we are not doing our job properly.

* ### Honest advice and top quality service.
We are happy to provide advice, free of charge. If what you are after isn't something we can help you with, we can recommend alternate solutions.

* ### Always be friendly
We are nice people, we like working with nice people. It's a two way thing. We will always aim to be friendly and personable with our clients.

We are a small agency compared to others, our prices reflect this. We don't have to worry about paying multiple staff and forking out for office space or other costly overheads. Just because our prices are slightly lower, does not mean we skimp on quality or service.

If you like the sound of how we work, please do get in touch via our contact form or via the telephone numbers in the footer. Email is our preferred contact method, we will always reply.

By working with us you can guarantee that your project will be completed to the highest standard, we build long-lasting relationships with our clients. Helping them to grow their brand and online presence.