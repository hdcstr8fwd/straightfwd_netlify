+++
comments = false
draft = false
noauthor = true
share = false
title = "Services"
type = "page"
[menu.main]
weight = 111
+++

We offer several services for your company. We take a great deal of pride in our work, striving to offer a service to clients that will provide the foundation for a great presence on the web. 

A website is an evolving product, If built and left alone it will fall into a bad state. As technology progresses quickly, what was once standard becomes obsolete.

## Web design / development

We can design and build your website from the ground up, to your specifications. We work with a number of different technologies. Cherry picking the best from our toolbox to build your website. 

This might be a custom solution build from the ground up, or an affordable and fast turnaround WordPress template and setup.

For our custom solutions we work with Hugo, Craft CMS and WordPress  depending on the project.

## Website management

If you are looking for someone to help you manage your website, we offer a number of solutions. 

We can help you with basic maintenance of your website to keep it running smoothly. This might range from small website alterations and software updates.

Building on this we can also manage content. From posting your content to building a bespoke content schedule to optimize the reach of your articles.
Marketing email development and delivery

Working with Mailchimp we can design build and distribute email campaigns for you and your business. Custom made solutions available or choose from our more affordable templates.
